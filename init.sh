#!/bin/bash
##
## Copyright (c) 2021 The Yaook Authors.
##
## This file is part of Yaook.
## See https://yaook.cloud for further info.
##
## Licensed under the Apache License, Version 2.0 (the "License");
## you may not use this file except in compliance with the License.
## You may obtain a copy of the License at
##
##     http://www.apache.org/licenses/LICENSE-2.0
##
## Unless required by applicable law or agreed to in writing, software
## distributed under the License is distributed on an "AS IS" BASIS,
## WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
## See the License for the specific language governing permissions and
## limitations under the License.
##

set -eux

mount -t proc proc /proc
mount -t sysfs sys /sys
mount -t tmpfs none /run
mount -t tmpfs none /tmp

mkdir /dev/pts
mount -t devpts devpts /dev/pts
rm /dev/ptmx
ln -s /dev/pts/ptmx /dev/ptmx

ln -s /proc/self/fd/ /dev/fd

rngd -r /dev/urandom

/etc/init.d/udev start

echo '127.0.1.1 uml' >> /etc/hosts
hostname uml

ip link set dev lo up
ip link set dev eth0 up
ifconfig eth0 10.0.2.15
route add default dev eth0

source /cmd

"${CMD[@]}"

echo "exit ${?}" > /exit

/sbin/halt -f
