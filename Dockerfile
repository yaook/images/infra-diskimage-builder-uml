##
## Copyright (c) 2021 The Yaook Authors.
##
## This file is part of Yaook.
## See https://yaook.cloud for further info.
##
## Licensed under the Apache License, Version 2.0 (the "License");
## you may not use this file except in compliance with the License.
## You may obtain a copy of the License at
##
##     http://www.apache.org/licenses/LICENSE-2.0
##
## Unless required by applicable law or agreed to in writing, software
## distributed under the License is distributed on an "AS IS" BASIS,
## WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
## See the License for the specific language governing permissions and
## limitations under the License.
##

ARG UBUNTU_VERSION=22.04
ARG KERNEL_VERSION=5.12

FROM ubuntu:$UBUNTU_VERSION as kernel_build

ENV DEBIAN_FRONTEND=noninteractive

RUN \
	apt-get update && \
	apt-get install git fakeroot build-essential ncurses-dev xz-utils libssl-dev bc wget flex bison libelf-dev -y && \
	apt-get install -y --no-install-recommends libarchive-tools

ARG KERNEL_VERSION

RUN \
	wget https://cdn.kernel.org/pub/linux/kernel/v5.x/linux-$KERNEL_VERSION.tar.xz && \
	tar -xf linux-$KERNEL_VERSION.tar.xz && \
	rm linux-$KERNEL_VERSION.tar.xz

WORKDIR linux-$KERNEL_VERSION
COPY KERNEL.config .config
RUN make ARCH=um oldconfig && make ARCH=um prepare
RUN make ARCH=um -j `nproc`
RUN mkdir /out && cp -f linux /out/linux


FROM ubuntu:$UBUNTU_VERSION

ENV DEBIAN_FRONTEND=noninteractive

RUN apt-get update \
    && apt-get install -y \
        cryptsetup-bin \
        curl \
        dosfstools \
        e2fsprogs \
        finit-sysv \
        gdisk \
        git \
        iproute2 \
        kpartx \
        lvm2 \
        mdadm \
        net-tools \
        python3 \
        python3-pip \
        qemu-utils \
        rng-tools \
        slirp \
        sudo \
        wget \
    && apt-get clean

RUN pip3 install --no-cache-dir diskimage-builder ironic-python-agent-builder

# UML does not support selinux, we need to disable this script for centos builds to finish
RUN sed -i '/^#!/a exit 0' /usr/local/lib/python3.*/dist-packages/diskimage_builder/elements/rpm-distro/cleanup.d/99-selinux-fixfiles-restore

# install kernel and scripts
COPY --from=kernel_build /out/linux /linux/linux
ADD entrypoint.sh entrypoint.sh
ADD init.sh init.sh

# size of the memory available to the UML kernel
ENV MEM 8G

# UML creates its memory as a memory-mapped file under TMPDIR.
# To improve performance it is recommended to override /umlshm with
# --tmpfs /umlshm:rw,nosuid,nodev,exec,size=8g
VOLUME /umlshm
ENV TMPDIR /umlshm

# cache for diskimage-builder downloads
VOLUME /dib_cache
ENV DIB_IMAGE_CACHE=/dib_cache

ENTRYPOINT [ "/entrypoint.sh" ]
CMD [ "bash" ]
