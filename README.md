<!--
Copyright (c) 2021 The Yaook Authors.
This file is part of Yaook.
See https://yaook.cloud for further info.
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
    http://www.apache.org/licenses/LICENSE-2.0
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
-->
# infra-diskimage-builder-uml

Image to run diskimage-builder in unprivileged containers, using User Mode Linux to provide an isolated block device layer.
Based on [diuid](https://github.com/weber-software/diuid), a Docker in User Mode Linux in Docker image.

## Usage

    user@host:~# docker build . -t infra-diskimage-builder-uml:latest
    user@host:~# docker run -it infra-diskimage-builder-uml:latest bash
    root@uml:/# disk-image-create [...]

Since the UML Kernel shares its root file system with the container, any volume mounted in the container can be accessed from UML.

## Drawbacks

* It's slow.
  You can add `--tmpfs /umlshm:rw,nosuid,nodev,exec,size=8g` to improve performance somewhat, but you can still expect a build to take 4 to 8 times as long as outside of UML.

* It does not support SELinux.
  If your image uses SELinux, you may have to fix file contexts.

## License

[Apache 2](LICENSE.txt)
